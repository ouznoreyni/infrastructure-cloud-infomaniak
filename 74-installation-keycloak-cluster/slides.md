%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗███████╗██╗   ██╗ ██████╗██╗      ██████╗  █████╗ ██╗  ██╗
██║ ██╔╝██╔════╝╚██╗ ██╔╝██╔════╝██║     ██╔═══██╗██╔══██╗██║ ██╔╝
█████╔╝ █████╗   ╚████╔╝ ██║     ██║     ██║   ██║███████║█████╔╝ 
██╔═██╗ ██╔══╝    ╚██╔╝  ██║     ██║     ██║   ██║██╔══██║██╔═██╗ 
██║  ██╗███████╗   ██║   ╚██████╗███████╗╚██████╔╝██║  ██║██║  ██╗
╚═╝  ╚═╝╚══════╝   ╚═╝    ╚═════╝╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝



-----------------------------------------------------------------------------------------------------------

# Keycloak : Create a cluster with ansible

<br>

Create a new role for our keycloak cluster

-----------------------------------------------------------------------------------------------------------

# Keycloak : Create a cluster with ansible

<br>

Change ou postgresql cluster to add database and user

```
postgresql_replication_pghba_entries:
  - { host: 'host', database: '{{ all_keycloak_database_user }}', user: 'all', address: '10.0.1.0/24', method: 'md5' }
postgresql_manage:
  users:
    - { name: "{{ all_keycloak_database_user }}", password: "{{ all_keycloak_database_password }}" }
  db_name:
    - "{{ all_keycloak_database_db }}"
  privileges:
    - { db: "{{ all_keycloak_database_host }}" , type: "database", user: "{{ all_keycloak_database_user }}", permissions: "ALL" }
  privileges_schema:
    - { schemas_name: "public", owner: "{{ all_keycloak_database_user }}", db: "{{ all_keycloak_database_db }}" }
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Create a cluster with ansible

<br>


In all.yml

```
    all_keycloak_database_user
    all_keycloak_database_password
    all_keycloak_database_host
    all_keycloak_admin_user
    all_keycloak_admin_user_password
    all_keycloak_database_db
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Create a cluster with ansible

<br>

Create a new group of server

```
module "keycloak" {
  source                      = "../modules/instance"
  instance_count              = 2
  instance_name               = "keycloak"
  instance_key_pair           = "default_key"
  instance_security_groups    = ["all_internal"]
  instance_network_internal   = var.network_internal_dev
  instance_ssh_key = var.ssh_public_key_default_user
  instance_flavor_name        = "a4-ram8-disk20-perf1"
  instance_default_user = var.default_user
  metadatas                   = {
    environment          = "dev",
    app         = "keycloak"
  }
}
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Create a cluster with ansible

<br>

Add some variables

```
keycloak_version: 24.0.3
keycloak_java_package: openjdk-17-jre-headless
keycloak_database_host: ""
keycloak_database_user: ""
keycloak_database_password: ""
keycloak_database_db: ""
keycloak_server_group: "meta-app_keycloak"
keycloak_java_tool: openjdk-17-jre-headless
keycloak_admin_user: ""
keycloak_admin_user_password: ""
keycloak_keystore_secret: secret
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Create a cluster with ansible

<br>

Edit /etc/hosts

```
- name: Update the /etc/hosts
  lineinfile:
    dest: "/etc/hosts"
    regexp: ".*\t{{ item }}\t{{ item }}"
    line: "{{ hostvars[item]['ansible_host'] }}\t{{ item }}\t{{ item }}"
    state: present
  when: ansible_hostname != item
  loop: "{{ groups[keycloak_server_group] }}"
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Create a cluster with ansible

<br>

Create user

```
- name: Create user keycloak
  user:
    name: keycloak
    system: yes
    shell: /sbin/nologin
    state: present
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Create a cluster with ansible

<br>

Create directories

```
- name: Create keycloak directories
  file:
    path: "{{ item }}"
    owner: keycloak
    group: keycloak
    mode: 0755
    state: directory
  loop:
  - /var/log/keycloak/
  - /opt/keycloak
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Create a cluster with ansible

<br>

Install Java

```
- name: Install openjdk package
  apt:
    name: "{{ keycloak_java_package }}"
    state: present
    update_cache: yes
    cache_valid_time: 3600
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Create a cluster with ansible

<br>

Check if keycloak exist

```
- name: check if keycloak dir already exists
  stat:
    path: /opt/keycloak/keycloak-{{ keycloak_version }}
  register: __keycloak_exists
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Create a cluster with ansible

<br>

Download keycloak

```
- name: Download Keycloak
  unarchive:
    src: https://github.com/keycloak/keycloak/releases/download/{{ keycloak_version }}/keycloak-{{ keycloak_version }}.tar.gz
    dest: /opt/keycloak
    remote_src: yes
    owner: keycloak
    group: keycloak
  when: __keycloak_exists.stat.exists == false
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Create a cluster with ansible

<br>

Add configuration

```
- name: Add keycloak configuration
  template:
    src: keycloak.conf.j2
    dest: "/opt/keycloak/keycloak-{{ keycloak_version }}/conf/keycloak.conf"
    owner: keycloak
    group: keycloak
    mode: 0750
  notify: restart_keycloak
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Create a cluster with ansible

<br>

Add quarkus configuration

```
- name: Add quarkus configuration
  template:
    src: quarkus.properties.j2
    dest: "/opt/keycloak/keycloak-{{ keycloak_version }}/conf/quarkus.properties"
    owner: keycloak
    group: keycloak
    mode: 0750
  notify: restart_keycloak
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Create a cluster with ansible

<br>

Add cache configuration

```
- name: Add ispn cache configuration
  template:
    src: cache-ispn.xml.j2
    dest: "/opt/keycloak/keycloak-{{ keycloak_version }}/conf/cache-ispn.xml"
    owner: keycloak
    group: keycloak
    mode: 0750
  notify: restart_keycloak
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Create a cluster with ansible

<br>

Add keycloak systemd service

```
- name: Add keycloak systemd service
  template:
    src: keycloak.service.j2
    dest: "/etc/systemd/system/keycloak.service"
    owner: root
    group: root
    mode: 0750
  notify: restart_keycloak
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Create a cluster with ansible

<br>

Check if the keystore already exists

```
- name: check if keystore already exists
  stat:
    path: /opt/keycloak/keycloak-{{ keycloak_version }}/conf/server.keystore
  register: __keystore_exists
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Create a cluster with ansible

<br>

Create the keystore if needed

```  
- name: create keystore if not exists
  shell: "/usr/bin/keytool -genkeypair -alias localhost -keyalg RSA -keysize 2048 -validity 7000 -keystore server.keystore -dname cn=ServerAdministrator,o=Acme,c=FR -keypass {{ keycloak_keystore_secret }} -storepass {{ keycloak_keystore_secret }}"
  args:
    chdir: /opt/keycloak/keycloak-{{ keycloak_version }}/conf/
  when: __keystore_exists.stat.exists == false
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Create a cluster with ansible

<br>

Start keycloak 

```
- meta: flush_handlers

- name: Start keycloak systemd service
  service:
    name: keycloak
    state: started
    enabled: yes
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Create a cluster with ansible

<br>

Add the handler

```
- name: restart_keycloak
  service:
    name: keycloak
    state: restarted
    daemon_reload: yes
    enabled: yes
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Create a cluster with ansible

<br>

Add the playbook

```
- name: install our keycloak
  become: yes
  hosts: meta-app_keycloak
  roles:
    - keycloak
    - consul/consul_services
  vars:
    keycloak_database_user: "{{ all_keycloak_database_user  }}"
    keycloak_database_password: "{{ all_keycloak_database_password }}"
    keycloak_database_host: "{{ all_keycloak_database_host }}"
    keycloak_database_db: "{{ all_keycloak_database_db: }}"
    keycloak_server_group: "meta-app_keycloak"
    keycloak_admin_user: "{{ all_keycloak_admin_user  }}"
    keycloak_admin_user_password: "{{ all_keycloak_admin_user_password  }}"
    keycloak_primary_server: keycloak1
    keycloak_keystore_secret: secret # need to change
    consul_services:
      - {
        name: "keycloak",
        type: "tcp",
        check_target: "127.0.0.1:8080",
        interval: "10s",
        port: 8080  ,
        tags: [
            "traefik.enable=true",
            "traefik.http.routers.router-keycloak.entrypoints=http,https",
            "traefik.http.routers.router-keycloak.rule=Host(`k.xavki.fr`)",
            "traefik.http.routers.router-keycloak.service=keycloak",
            "traefik.http.routers.router-keycloak.middlewares=to_https@file",
            "traefik.http.routers.router-keycloak.tls.certresolver=certs_gen"
          ]
        }
```

-----------------------------------------------------------------------------------------------------------

# Keycloak : Create a cluster with ansible

<br>

Change the basic auth

```
  [http.middlewares]
    [http.middlewares.auth.basicAuth]
      users = [
      "xavki:$apr1$rnqfuhkt$u7ubv2bBKQYNscWTw2xy./"
      ]
      removeHeader = true
```

