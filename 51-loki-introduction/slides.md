%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Loki : introduction

<br>

The troubleshooting way ???

  alert > go to vizualisation > select timestamp > check logs > analyze traces 


Github : https://github.com/grafana/loki
Official website : https://grafana.com/oss/loki/
Language : Golang

-----------------------------------------------------------------------------------------------------------                                       

# Loki : introduction

<br>

Loki = centralization or log storage

Some observations :

    * prometheus create a new standard for log storage and query language (openmetrics / metricsql)

    * ELK (Elasticsearch Logstash Kibana) stack consume lot of resources (memory)
        * due to inverted indexes

    * some lightweight log collectors emerge from kubernetes ecosystem (fluentbit, fluentd...)

    * Grafana Labs wants to expand its data world to logs (and after tracing...)

    * Log storage takes volumes and costs

Grafana Labs created loki

-----------------------------------------------------------------------------------------------------------                                       

# Loki : introduction

<br>

Loki :

    * simple service (or multiple micro-services)

    * less resources (less indexation = less memory usage)

    * indexation only on timestamp, metadatas and labels (not on messages)

    * easy to maintain (not like elasticsearch ~ )

    * uses logql = some similarities with openmetrics

    * easy to centralize logs and metrics (elasticsearch not so easy to insert in grafana)


-----------------------------------------------------------------------------------------------------------                                       

# Loki : introduction

<br>

Principles :

    collector > loki > grafana

Logs collector to send datas/logs to loki :

    * promtail

    * fluentbit

    * fluentd

    * filebeat

    * vector (++)

    * grafana agent

    * some code librairies
        (https://pypi.org/project/python-logging-loki/)

-----------------------------------------------------------------------------------------------------------                                       

# Loki : introduction

<br>

Three modes : 

    * monolith mode (one binary)

    * simple scalable

    * micro-service

A log line for loki :

```
{job="syslog", instance="host1"} 00:00:00 i'm a syslog!
```

-----------------------------------------------------------------------------------------------------------                                       

# Loki : introduction

<br>

Loki service architecture

  Write path : 

      * distributor :
          receive data from clients
          split data into chunks
          load balancer before ;)
          stateless

      * ingester :
          write data into long term storage
          used also in read path

-----------------------------------------------------------------------------------------------------------                                       

# Loki : introduction

<br>

  Read path :

      * query-frontend :
          optional
          send requests to querier
          buffer for requests

      * caches : results / index / chunks

      * queriers : 
          process LogQL queries
          download data from ingester first
          if no data in ingester memory go to datastore

      * ruler :
          alert computation

      * compactor :
          manage s3 storage
          retention
          manage index size

-----------------------------------------------------------------------------------------------------------                                       

# Loki : introduction

<br>

Two types of data : 

      * indexes : link between tags and chunks (part of data)

      * chunks : data is splitted into parts to send it in storage

Since 2.0 can be in same storage (Boltdb for indexes)

-----------------------------------------------------------------------------------------------------------                                       

# Loki : introduction

<br>

And more :

    * replication factor

    * compatible s3 storage or filesystem (optionnal for indexes - ex cassandra...)

    * can be use with grafana / logcli / alertmanager

    * docker plugin : driver loki
