%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Consul - Installation manuel

<br>

Change directory to isolate vpn and the infrastructure

```
source          = "./../modules/network"
```

Create a new directory for the insrastructure

    * create directory

    * copy previous provider and change the state address

    * create a new instance with our module (consul)

Note : change dns (cloud init)

-----------------------------------------------------------------------------------------------------------                                       

# Consul - Installation manuel

<br>

Security group

```
resource "openstack_networking_secgroup_v2" "consul" {
  name        = "consul"
  description = "consul"
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Consul - Installation manuel

<br>

Security group

```
resource "openstack_networking_secgroup_rule_v2" "secgroup_consul_dns_tcp" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8600
  port_range_max    = 8600
  remote_ip_prefix  = var.network_subnet_cidr
  security_group_id = openstack_networking_secgroup_v2.consul.id
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Consul - Installation manuel

<br>

Security group

```
resource "openstack_networking_secgroup_rule_v2" "secgroup_consul_dns_udp" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 8600
  port_range_max    = 8600
  remote_ip_prefix  = var.network_subnet_cidr
  security_group_id = openstack_networking_secgroup_v2.consul.id
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Consul - Installation manuel

<br>

Security group

```
resource "openstack_networking_secgroup_rule_v2" "secgroup_consul_http_grpc" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8500
  port_range_max    = 8503
  remote_ip_prefix  = var.network_subnet_cidr
  security_group_id = openstack_networking_secgroup_v2.consul.id
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Consul - Installation manuel

<br>

Security group

```
resource "openstack_networking_secgroup_rule_v2" "secgroup_consul_wlan_tcp" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8300
  port_range_max    = 8302
  remote_ip_prefix  = var.network_subnet_cidr
  security_group_id = openstack_networking_secgroup_v2.consul.id
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Consul - Installation manuel

<br>

Security group

```
resource "openstack_networking_secgroup_rule_v2" "secgroup_consul_wlan_udp" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = 8300
  port_range_max    = 8302
  remote_ip_prefix  = var.network_subnet_cidr
  security_group_id = openstack_networking_secgroup_v2.consul.id
}
```


-----------------------------------------------------------------------------------------------------------                                       

# Consul - Installation manuel

<br>

Github : https://github.com/hashicorp/consul


-----------------------------------------------------------------------------------------------------------                                       

# Consul - Installation manuel

<br>

Variables

```
IP=$(hostname -I)
VERSION=1.18.0
```

Install utils

```
apt-get update -qq
apt-get install -qq -y wget unzip dnsutils
```

-----------------------------------------------------------------------------------------------------------                                       

# Consul - Installation manuel

<br>

Directories and user

```
groupadd --system consul
useradd -s /sbin/nologin --system -g consul consul
mkdir -p /var/lib/consul
chown -R consul:consul /var/lib/consul
chmod -R 750 /var/lib/consul
mkdir /etc/consul.d
chown -R consul:consul /etc/consul.d
wget -q https://releases.hashicorp.com/consul/${VERSION}/consul_${VERSION}_linux_amd64.zip
unzip consul_${VERSION}_linux_amd64.zip
mv consul /usr/local/bin/
chmod +x -R /usr/local/bin/
```

-----------------------------------------------------------------------------------------------------------                                       

# Consul - Installation manuel

<br>

Master settings


```
echo '{
    "advertise_addr": "'${IP}'",
    "bind_addr": "'${IP}'",
    "bootstrap_expect": '3',
    "client_addr": "0.0.0.0",
    "datacenter": "xavki",
    "data_dir": "/var/lib/consul",
    "domain": "consul",
    "enable_script_checks": true,
    "dns_config": {
        "enable_truncate": true,
        "only_passing": true
    },
    "enable_syslog": true,
    "leave_on_terminate": true,
    "log_level": "INFO",
    "rejoin_after_leave": true,
    "retry_join": [
        "consul1",
        "consul2",
        "consul3"
    ],
    "server": true,
    "ui": true
}' > /etc/consul.d/config.json
```

-----------------------------------------------------------------------------------------------------------                                       

# Consul - Installation manuel

<br>

Agent settings

```
echo '{
    "advertise_addr": "'${IP}'",
    "bind_addr": "'${IP}'",
    "client_addr": "0.0.0.0",
    "datacenter": "xavki",
    "data_dir": "/var/lib/consul",
    "domain": "xavki",
    "enable_script_checks": true,
    "dns_config": {
        "enable_truncate": true,
        "only_passing": true
    },
    "enable_syslog": true,
    "leave_on_terminate": true,
    "log_level": "INFO",
    "rejoin_after_leave": true,
    "retry_join": [
        "consul1",
        "consul2",
        "consul3"
    ],
    "server": false
}' > /etc/consul.d/config.json
```

-----------------------------------------------------------------------------------------------------------                                       

# Consul - Installation manuel

<br>

Consul systemd service

```
echo '[Unit]
Description=Consul Service Discovery Agent
Documentation=https://www.consul.io/
After=network-online.target
Wants=network-online.target

[Service]
Type=simple
User=consul
Group=consul
ExecStart=/usr/local/bin/consul agent \
  -node='$IP' \
  -config-dir=/etc/consul.d

ExecReload=/bin/kill -HUP $MAINPID
KillSignal=SIGINT
TimeoutStopSec=5
Restart=on-failure
SyslogIdentifier=consul

[Install]
WantedBy=multi-user.target' > /etc/systemd/system/consul.service
```

-----------------------------------------------------------------------------------------------------------                                       

# Consul - Installation manuel

<br>

Start and enable consul

```
systemctl enable consul
service consul start
```
